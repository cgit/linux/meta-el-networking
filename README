meta-el-networking
===================================================================

This layer contains distro customizations specific to the Enea Linux
Networking profile releases.

Dependencies
============

This layer depends on:

  URI: git://git.yoctoproject.org/poky
  branch: master

  URI: git://git.enea.com/linux/meta-el-common
  branch: master

  URI: git://git.enea.com/linux/meta-enea-networking
  branch: master



Source code
===========

git://git.enea.com/linux/meta-el-networking.git


Patches
=======

Please submit any patches against the el-networking layer to the
following mailing list: linux@lists.enea.com

Maintainers:
Adrian Dudau <adrian.dudau@enea.com>
Martin Borg <martin.borg@enea.com>

Table
=================

  I. Adding the el-networking layer to your build
 II. Misc


I. Adding the el-networking layer to your build
=================================================

In order to use this layer, you need to make the build system aware of
it.

Assuming the el-networking layer exists at the top-level of your
yocto build tree, you can add it to the build system by adding the
location of the el-networking layer to bblayers.conf, along with any
other layers needed. e.g.:


  BBLAYERS ?= " \
      /path/to/yocto/meta \
      /path/to/yocto/meta-poky \
      /path/to/yocto/meta-el-common \
      /path/to/yocto/meta-enea-networking \
      /path/to/yocto/meta-el-networking \
      "

II. Misc
========
