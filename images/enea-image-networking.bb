DESCRIPTION = "Base image for the Networking profile"

require images/enea-image-common.inc
require images/enea-image-networking-common.inc

IMAGE_ROOTFS_EXTRA_SPACE = "131072"
IMAGE_OVERHEAD_FACTOR = "2"
