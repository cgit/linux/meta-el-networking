DESCRIPTION = "Full featured image for the Networking profile"

require images/enea-image-common.inc
require images/enea-image-networking-common.inc
require images/enea-image-extra.inc

IMAGE_ROOTFS_EXTRA_SPACE = "1048576"
IMAGE_OVERHEAD_FACTOR = "1.5"
